(include-book "centaur/esim/tutorial/intro" :dir :system)
(include-book "centaur/gl/bfr-satlink" :dir :system)
(include-book "centaur/bitops/top" :dir :system)
(tshell-ensure)
(plev)

					; SAT solver
(local
 (progn
   (defun my-glucose-config ()
     (declare (xargs :guard t))
     (satlink::make-config :cmdline "/Users/jerryhardee/amber/simp"
                           :verbose t
                           :mintime 1/2
                           :remove-temps t))

   (defattach gl::gl-satlink-config my-glucose-config)))

					; Load RTL source
(defmodules *translation*
    (vl2014::make-vl-loadconfig
     :start-files (list "a23_alu.v")))

(defconst *a23-alu-vl*
  (vl2014::vl-find-module "a23_alu"
			  (vl2014::vl-design->mods
			   (vl2014::vl-translation->good *translation*))))

(vl2014::vl-warnings-to-string (vl2014::vl-module->warnings *a23-alu-vl*))

(defconst *a23-alu*
  (vl2014::vl-module->esim *a23-alu-vl*))

(defstv test-vector
    :mod *a23-alu*
    :inputs
    '(("i_a_in" a)
      ("i_b_in" b)
      ("i_barrel_shift_carry" barrel-shift-carry)
      ("i_status_bits_carry" carry-in)
      ("i_function" function))

    :outputs
    '(("o_out" res)
      ("o_flags" flags)))

(defmacro default-bindings ()
  `(gl::auto-bindings (:nat function 9)
		      (:mix (:nat a 32)
			    (:nat b 32))
		      (:nat barrel-shift-carry 1)
		      (:nat carry-in 1)))

					; ALU "opcodes", the lower 4 bits of instruction input.
(defconst *op-b* 0)
(defconst *op-add* 1)
(defconst *op-zero-extend-b-16* 2)
(defconst *op-zero-extend-b-8* 3)
(defconst *op-sign-extend-b-16* 4)
(defconst *op-sign-extend-b-8* 5)
(defconst *op-xor* 6)
(defconst *op-or* 7)
;; Actually, any opcode greater than or equal to 8 is op-and.
(defconst *op-and* 8)

					; ALU modifier bit posiitions; modifier bits are the upper 5 bits
					; of instruction input.
;; Swap the roles of a and b.
(defconst *mod-swap* 8)
;; Invert b (after swapping, if applicable).
(defconst *mod-inv-b* 7)
;; When high, use carry-in. When low, use next modifier bit instead.
(defconst *mod-override-carry-in-n* 6)
;; When previous modifier bit is low, this is the carry in.
(defconst *mod-set-carry-in* 5)
;; When low, use adder's carry out. Else, barrel-shift-carry.
(defconst *mod-carry-out* 4)


					; Flags bit positions in flag output.
(defconst *flag-negative* 3)
(defconst *flag-zero* 2)
(defconst *flag-carry* 1)
(defconst *flag-overflow* 0)

					; Sign helper.
(defun neg-bit-set-p (x)
  (declare (type (unsigned-byte 32) x))
  (logbitp 31 x))

					; Theorems
					; Prove correctness for "vanilla" instructions,
					; that is, without any instruction modifier bits.
(defmacro default-hyps ()
  `(and (unsigned-byte-p 9 function)
	(unsigned-byte-p 32 a)
	(unsigned-byte-p 32 b)	      
	(unsigned-byte-p 1 barrel-shift-carry)
	(unsigned-byte-p 1 carry-in)))



(defmacro test-vector-inputs (&key (function `function) (a `a) (b `b) (barrel-shift-carry `barrel-shift-carry)
				(carry-in `carry-in))
  `(list (cons 'function ,function)
	 (cons 'a ,a)
	 (cons 'b ,b)
	 (cons 'barrel-shift-carry ,barrel-shift-carry)
	 (cons 'carry-in ,carry-in)))

(defmacro cdr-assoc (k x)
  `(cdr (assoc ,k ,x)))

(defmacro def-alu-thm-vanilla (name &key function-p res)
  `(def-gl-thm ,name
       :hyp (and (default-hyps)
		 ;; no modifier bits in instruction:
		 (not (logtest function #b111110000))
		 ,function-p)
       :concl (b* ((in-alist (test-vector-inputs))
		   (out-alist (stv-run (test-vector) in-alist))
		   (res (cdr-assoc 'res out-alist))
		   (flags (cdr-assoc 'flags out-alist))
		   ;; For "vanilla" instructions, carry should be computed the same way
		   ;; every time:		
		   (expected-carry (< (loghead 32 (+ a b))
				      (+ a b)))
		   ;; Overflow should be nil, unless we're adding.
		   (expected-overflow (if (equal function *op-add*)
					  (or (and (neg-bit-set-p a)
						   (neg-bit-set-p b)
						   (not (neg-bit-set-p res)))
					      (and (not (neg-bit-set-p a))
						   (not (neg-bit-set-p b))
						   (neg-bit-set-p res)))
					  nil)))
		  ;; Helpful debugging:
		  (cw "Expected res: ~s0~%" (str::hexify ,res))
		  (cw "Expected overflow: ~s0~%" (str::hexify expected-overflow))
		  (cw "Expected carry: ~s0~%" (str::hexify expected-carry))
		  ;; Actual test:
		  (and (equal res ,res)
		       (equal (logbitp *flag-overflow* flags)
			      expected-overflow)
		       (equal (logbitp *flag-carry* flags)
			      expected-carry)))
       :g-bindings (default-bindings)))
  
(def-alu-thm-vanilla op-b-correct
    :function-p (equal function *op-b*)
    :res b)

(def-alu-thm-vanilla op-add-correct
    :function-p (equal function *op-add*)
    :res (loghead 32 (+ a b)))

(def-alu-thm-vanilla op-zero-extend-b-16-correct
    :function-p (equal function *op-zero-extend-b-16*)
    :res (logand b #ux0000_FFFF))

(def-alu-thm-vanilla op-zero-extend-b-8-correct
    :function-p (equal function *op-zero-extend-b-8*)
    :res (logand b #ux0000_00FF))

(def-alu-thm-vanilla op-sign-extend-b-16-correct
    :function-p (equal function *op-sign-extend-b-16*)
    :res (logextu 32 16 b))

(def-alu-thm-vanilla op-sign-extend-b-8-correct
    :function-p (equal function *op-sign-extend-b-8*)
    :res (logextu 32 8 b))

(def-alu-thm-vanilla op-xor-correct
    :function-p (equal function *op-xor*)
    :res (logxor a b))

(def-alu-thm-vanilla op-or-correct
    :function-p (equal function *op-or*)
    :res (logior a b))

(def-alu-thm-vanilla op-and-correct
    :function-p (>= function *op-and*)
    :res (logand a b))

					; Prove correctness of negative and zero flags that don't depend on the instruction input.
(def-gl-thm negative-flag-correct 	; That is, set if and only if result is negative.
    :hyp (default-hyps)
    :concl (let* ((in-alist (test-vector-inputs))
		  (out-alist (stv-run (test-vector) in-alist))
		  (res (cdr-assoc 'res out-alist))
		  (flags (cdr-assoc 'flags out-alist)))
	     (equal (logbitp *flag-negative* flags)
		    (neg-bit-set-p res)))
    :g-bindings (default-bindings))

(def-gl-thm zero-flag-correct		; That is, set if and only if result is zero.
    :hyp (default-hyps)
    :concl (let* ((in-alist (test-vector-inputs))
		  (out-alist (stv-run (test-vector) in-alist))
		  (res (cdr-assoc 'res out-alist))
		  (flags (cdr-assoc 'flags out-alist)))
	     (equal (logbitp *flag-zero* flags)
		    (zp res)))
    :g-bindings (default-bindings))

(defun toggle-bit (bit-pos word)
  (logxor word (ash 1 bit-pos)))


					; Prove that extra modifier bits on instruction word are
					; correctly interpreted.
(def-gl-thm toggling-mod-swap-swaps-a-and-b
    :hyp (default-hyps)
    :concl (let* ((in-alist1 (test-vector-inputs :function (toggle-bit *mod-swap* function)))
		  (in-alist2 (test-vector-inputs :a b :b a))
		  (out-alist1 (stv-run (test-vector) in-alist1))
		  (res1 (cdr-assoc 'res out-alist1))
		  (flags1 (cdr-assoc 'flags out-alist1))
		  (out-alist2 (stv-run (test-vector) in-alist2))
		  (res2 (cdr-assoc 'res out-alist2))
		  (flags2 (cdr-assoc 'flags out-alist2)))
	     (and (equal res1 res2)
		  (equal flags1 flags2)))
    :g-bindings (default-bindings))


(def-gl-thm mod-inv-b-inverts-b-after-any-swap
    :hyp (default-hyps)
    :concl (let* ((in-alist1 (test-vector-inputs :function (install-bit *mod-inv-b* 1 function)))
		  (in-alist2 (test-vector-inputs :function (install-bit *mod-inv-b* 0 function)
						 :a (if (logbitp *mod-swap* function)
							(lognotu 32 a)
							a)
						 :b (if (logbitp *mod-swap* function)
							b
							(lognotu 32 b))))
		  (out-alist1 (stv-run (test-vector) in-alist1))
		  (res1 (cdr-assoc 'res out-alist1))
		  (flags1 (cdr-assoc 'flags out-alist1))
		  (out-alist2 (stv-run (test-vector) in-alist2))
		  (res2 (cdr-assoc 'res out-alist2))
		  (flags2 (cdr-assoc 'flags out-alist2)))
	     (and (equal res1 res2)
		  (equal flags1 flags2)))
    :g-bindings (default-bindings))

(def-gl-thm mod-carry-out-does-not-affect-result-and-other-flags
    :hyp (default-hyps) 
    :concl (let* ((in-alist1 (test-vector-inputs :function (install-bit *mod-carry-out* 1 function)))
		  (in-alist2 (test-vector-inputs :function (install-bit *mod-carry-out* 0 function)))
		  (out-alist1 (stv-run (test-vector) in-alist1))
		  (res1 (cdr-assoc 'res out-alist1))
		  (flags1 (cdr-assoc 'flags out-alist1))
		  (out-alist2 (stv-run (test-vector) in-alist2))
		  (res2 (cdr-assoc 'res out-alist2))
		  (flags2 (cdr-assoc 'flags out-alist2)))
	     (and (equal res1 res2)
		  (equal (install-bit *flag-carry* 0 flags1)
			 (install-bit *flag-carry* 0 flags2))))
    :g-bindings (default-bindings))

(def-gl-thm mod-carry-out-passes-barrel-shift-carry-through
    :hyp (default-hyps)

    :concl (b* ((in-alist (test-vector-inputs :function (install-bit *mod-carry-out* 1 function)))
		(out-alist (stv-run (test-vector) in-alist))
		(flags (cdr-assoc 'flags out-alist)))
	       (equal (logbitp *flag-carry* flags)
		      (not (zp barrel-shift-carry))))
    :g-bindings (default-bindings))

(def-gl-thm mod-override-carry-in-overrides-carry-in-when-low-with-mod-set-carry-in
    :hyp (default-hyps)
    :concl (let* ((in-alist1 (test-vector-inputs
			      :function (install-bit *mod-override-carry-in-n* 0 function)))
		  (in-alist2 (test-vector-inputs :function (install-bit *mod-override-carry-in-n* 1 function)
						 :carry-in (if (logbitp *mod-set-carry-in* function)
									1
									0)))
		  (out-alist1 (stv-run (test-vector) in-alist1))
		  (res1 (cdr-assoc 'res out-alist1))
		  (flags1 (cdr-assoc 'flags out-alist1))
		  (out-alist2 (stv-run (test-vector) in-alist2))
		  (res2 (cdr-assoc 'res out-alist2))
		  (flags2 (cdr-assoc 'flags out-alist2)))
	     (and (equal res1 res2)
		  (equal flags1 flags2)))
    :g-bindings (default-bindings))

(def-gl-thm mod-override-carry-in-does-not-override-when-high
    :hyp (and (default-hyps)
	      (logbitp *mod-override-carry-in-n* function))
    :concl (let* ((in-alist1 (test-vector-inputs :function (install-bit *mod-set-carry-in* 1 function)))
		  (in-alist2 (test-vector-inputs :function (install-bit *mod-set-carry-in* 0 function)))
		  (out-alist1 (stv-run (test-vector) in-alist1))
		  (res1 (cdr-assoc 'res out-alist1))
		  (flags1 (cdr-assoc 'flags out-alist1))
		  (out-alist2 (stv-run (test-vector) in-alist2))
		  (res2 (cdr-assoc 'res out-alist2))
		  (flags2 (cdr-assoc 'flags out-alist2)))
	     (and (equal res1 res2)
		  (equal flags1 flags2)))
    :g-bindings (default-bindings))
