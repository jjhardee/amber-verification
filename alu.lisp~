(include-book "centaur/esim/tutorial/intro" :dir :system)
(include-book "centaur/gl/bfr-satlink" :dir :system)
(tshell-ensure)
(plev)

					; SAT solver
(local
 (progn
   (defun my-glucose-config ()
     (declare (xargs :guard t))
     (satlink::make-config :cmdline "/Users/jerryhardee/amber/simp"
                           :verbose t
                           :mintime 1/2
                           :remove-temps t))

   (defattach gl::gl-satlink-config my-glucose-config)))

					; Load RTL source
(defmodules *translation*
    (vl2014::make-vl-loadconfig
     :start-files (list "a23_alu.v")))

(defconst *a23-alu-vl*
  (vl2014::vl-find-module "a23_alu"
			  (vl2014::vl-design->mods
			   (vl2014::vl-translation->good *translation*))))

(vl2014::vl-warnings-to-string (vl2014::vl-module->warnings *a23-alu-vl*))

(defconst *a23-alu*
  (vl2014::vl-module->esim *a23-alu-vl*))

					; ALU "opcodes"
(defconst *op-b* '(0))
(defconst *op-add* '(1))
(defconst *op-zero-extend-b-16* '(2))
(defconst *op-zero-extend-b-8* '(3))
(defconst *op-sign-extend-b-16* '(4))
(defconst *op-sign-extend-b-8* '(5))
(defconst *op-xor* '(6))
(defconst *op-or* '(7))
(defconst *op-and* '(8 9 10 11 12 13 14 15))

					; ALU modifier bit-masks
(defconst *mod-swap* #b10000000)
(defconst *mod-inv-b* #b01000000)
(defconst *mod-set-carry-in* #b00100000)
(defconst *mod-barrel-shift-carry-out* #b00010000)

(defstv test-vector
    :mod *a23-alu*
    :inputs
    '(("i_a_in" a)
      ("i_b_in" b)
      ("i_barrel_shift_carry" barrel-shift-carry)
      ("i_status_bits_carry" status-bits-carry)
      ("i_function" function))

    :outputs
    '(("o_out" res)
      ("o_flags" flags)))

					; Flags bit-masks.
(defconst *flag-negative* #b1000)
(defconst *flag-zero* #b0100)
(defconst *flag-carry* #b0010)
(defconst *flag-overflow* #b0001)

					; Sign helper.
(defconst *num-negative* #ux8000_0000)
(defun neg-bit-set-p (x)
  (declare (type (unsigned-byte 32) x))
  (logtest x *num-negative*))

					; Theorems
					; Prove correctness for "vanilla" instructions,
					; that is, without any modifier bits.
 (defmacro def-alu-thm-vanilla (name &key op-list res overflow carry)
  `(def-gl-thm ,name
       :hyp (and (test-vector-autohyps)
		 (member function ,op-list))
       :concl (b* ((in-alist (list (cons 'function function)
				   (cons 'a a)
				   (cons 'b b)
				   (cons 'barrel-shift-carry barrel-shift-carry)
				   (cons 'status-bits-carry status-bits-carry)))
		   (out-alist (stv-run (test-vector) in-alist))
		   (res (cdr (assoc 'res out-alist)))
		   (flags (cdr (assoc 'flags out-alist))))
		  (cw "Expected res: ~s0~%" (str::hexify ,res))		  
		  (cw "Expected overflow: ~s0~%" (str::hexify ,overflow))
		  (cw "Expected carry: ~s0~%" (str::hexify ,carry))
		  (and (equal res ,res)
		       (equal (logtest flags *flag-overflow*)
			      ,overflow)
		       (equal (logtest flags *flag-carry*)
			      ,carry)))
       :g-bindings (gl::auto-bindings (:nat function 8)
				      (:mix (:nat a 32)
					    (:nat b 32))
				      (:nat barrel-shift-carry 1)
				      (:nat status-bits-carry 1))))
 
(def-alu-thm-vanilla op-b-correct
    :op-list *op-b*
    :res b
    :overflow nil
    :carry (>= (+ a b) (expt 2 32)))

(def-alu-thm-vanilla op-add-correct
    :op-list *op-add*
    :res (mod (+ a b)
	      (expt 2 32))
    :overflow (or (and (neg-bit-set-p a)
		       (neg-bit-set-p b)
		       (not (neg-bit-set-p res)))
		  (and (not (neg-bit-set-p a))
		       (not (neg-bit-set-p b))
		       (neg-bit-set-p res)))
    :carry (>= (+ a b)
	       (expt 2 32)))

(def-alu-thm-vanilla op-zero-extend-b-16-correct
    :op-list *op-zero-extend-b-16*
    :res (logext 16 b)
    :overflow nil
    :carry (>= (+ a b)
	       (expt 2 32)))

(def-alu-thm-vanilla op-zero-extend-b-8-correct
    :op-list *op-zero-extend-b-8*)

(def-alu-thm-vanilla op-sign-extend-b-16-correct
    :op-list *op-sign-extend-b-16*)

(def-alu-thm-vanilla op-sign-extend-b-8-correct
    :op-list *op-sign-extend-b-8*)

(def-alu-thm-vanilla op-xor-correct
    :op-list *op-xor*)

(def-alu-thm-vanilla op-or-correct
    :op-list *op-or*)

(def-alu-thm-vanilla op-and-correct
    :op-list *op-and*)


					; Prove correctness of flags that don't depend on operation performed.
(def-gl-thm negative-flag-correct
    :hyp (and (unsigned-byte-p 8 function)
	      (unsigned-byte-p 32 a)
	      (unsigned-byte-p 32 b)
	      (unsigned-byte-p 1 barrel-shift-carry)
	      (unsigned-byte-p 1 status-bits-carry)
	      )
    :concl (let* ((in-alist (list (cons 'function function)
				  (cons 'a a)
				  (cons 'b b)
				  (cons 'barrel-shift-carry barrel-shift-carry)
				  (cons 'status-bits-carry status-bits-carry)))
		  (out-alist (stv-run (test-vector) in-alist))
		  (res (cdr (assoc 'res out-alist)))
		  (flags (cdr (assoc 'flags out-alist))))
	     (equal (logtest flags *flag-negative*)
		    (logtest res *num-negative*)))
    :g-bindings (gl::auto-bindings (:nat function 8)
				   (:mix (:nat a 32)
					 (:nat b 32))
				   (:nat barrel-shift-carry 1)
				   (:nat status-bits-carry 1)
				   ))

(def-gl-thm zero-flag-correct
    :hyp (and (unsigned-byte-p 8 function)
	      (unsigned-byte-p 32 a)
	      (unsigned-byte-p 32 b)
	      (unsigned-byte-p 1 barrel-shift-carry)
	      (unsigned-byte-p 1 status-bits-carry)
	      )
    :concl (let* ((in-alist (list (cons 'function function)
				  (cons 'a a)
				  (cons 'b b)
				  (cons 'barrel-shift-carry barrel-shift-carry)
				  (cons 'status-bits-carry status-bits-carry)))
		  (out-alist (stv-run (test-vector) in-alist))
		  (res (cdr (assoc 'res out-alist)))
		  (flags (cdr (assoc 'flags out-alist))))
	     (equal (logtest flags *flag-zero*)
		    (zp res)))
    :g-bindings (gl::auto-bindings (:nat function 8)
				   (:mix (:nat a 32)
					 (:nat b 32))
				   (:nat barrel-shift-carry 1)
				   (:nat status-bits-carry 1)
				   ))
